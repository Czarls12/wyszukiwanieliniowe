package pl.karol.barczuk;

import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class Car implements Comparable<Car> {

    String mark;
    String model;



    public Car(String mark, String model) {
        this.mark = mark;
        this.model = model;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int compareTo(Car o) {
        int result = -1;
        if (getModel() == o.getModel()
                && getMark() == o.getMark()) {
            result = 0;
        }
        return result;
    }
}
