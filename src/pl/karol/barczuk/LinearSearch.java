package pl.karol.barczuk;

import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class LinearSearch<T extends Comparable<T>> extends Search<T> {

    @Override
    public int doSearch(List<T> list, T searchElement) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(searchElement) == 0) {
                //System.out.println("Porownywany element znajduje sie na pozycji " + i);
              return i;
//            } else if (i == list.size() - 1) {
//                System.out.println(-1);
            }
        }
                return -1;
    }
}
