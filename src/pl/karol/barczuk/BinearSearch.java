package pl.karol.barczuk;

import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class BinearSearch<T extends Comparable<T>> extends Search<T> {


    @Override
    public int doSearch(List<T> list, T searchElement) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(searchElement) == 0) {

                return i;
            }
        }
        return -1;
    }
}
